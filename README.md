<div id="top"></div>

<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://gitlab.com/tmccubbin/schedule-bot/-/blob/main/images/omnivox_logo.png">
    <img src="./images/omnivox_logo.png" alt="Logo" width="80" height="80">
  </a>

  <h3 align="center">Schedule Bot</h3>

  <p align="center">
    A bot to display when your next class is scheduled
    <br />
    <a href="https://gitlab.com/tmccubbin/schedule-bot"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    ·
    <a href="https://gitlab.com/tmccubbin/schedule-bot/-/issues">Report Bug</a>
    ·
    <a href="https://gitlab.com/tmccubbin/schedule-bot/-/issues">Request Feature</a>
  </p>
</div>

<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
</details>

<!-- ABOUT THE PROJECT -->
## About The Project
<div align="center">
    <img src="./images/example_discord.png" alt="Preview of bot in Discord" width="185px" height="321px"/>
</div>


This projects fetchs your next class and displays it as a bot in Discord.

Can be configured for any schedule as long as the JSON format stays the same.

<p align="right">(<a href="#top">back to top</a>)</p>

### Built With

* [Node.js](https://nodejs.org/)
* [Discord.js](https://discord.js.org/)
* [dotenv](https://www.npmjs.com/package/dotenv)

<p align="right">(<a href="#top">back to top</a>)</p>


<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[product-screenshot]: ./images/example_discord.png